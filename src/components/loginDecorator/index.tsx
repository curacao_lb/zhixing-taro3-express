import { useEffect } from 'react'

import tools from '@/common/tools'

function isLogin(WrappedComponent) {
  return () => {
    useEffect(() => {
      const userInfo = tools.getStorageSyncWithTime('userInfo')
      if (!userInfo?.userPhone) {
        // 未登录
        tools.navigateTo({
          url: '/pages/login/login'
        })
      }
    }, [])
    return <WrappedComponent />
  }
}

export default isLogin
