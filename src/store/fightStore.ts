import dayjs from 'dayjs'
import { atom } from 'recoil'

const INIT_STATE = {
  departureCityId: 2,
  departureCityName: '上海',
  departureAirportName: '虹桥机场',
  destinationCityId: 1,
  destinationCityName: '北京',
  destinationAirportName: '大兴机场',
  cityType: 'depart',
  dapartureDate: dayjs().format("YYYY-MM-DD") // 起飞时间
}

const flightState = atom({
  key: 'GLOBAL_STATE',
  default: INIT_STATE
})

export default flightState
