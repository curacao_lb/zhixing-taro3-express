import { atom } from 'recoil'

import tools from '@/common/tools'

const initUserInfo = () => {
  const userInfo = tools.getStorageSyncWithTime('userInfo')
  return {
    isLogin: !!userInfo?.userPhone, // 通过是否有手机号判断是否登录
    userPhone: userInfo?.userPhone,
    nickName: userInfo?.nickName
  }
}

const userInfo = atom({
  key: 'USER_INFO',
  default: initUserInfo()
})

export default userInfo
