import { View } from "@tarojs/components";
import { useState } from "react";
import "./index.scss";

import FlightIndex from "../flight/index";
import NoExploit from "../../components/NoExploit";

const DEFAULT_TAB_LIST = [
  {
    title: "机票",
    tab: "flight",
    index: 0,
  },
  {
    title: "火车票",
    tab: "train",
    index: 1,
  },
  {
    title: "酒店",
    tab: "hotel",
    index: 2,
  },
  {
    title: "汽车票",
    tab: "bus",
    index: 3,
  },
];

export default function Index() {
  const [current, setCurrent] = useState(0);

  const innerStyle = {
    width: `${100 / DEFAULT_TAB_LIST.length}%`,
    transform: `translateX(${current * 94 + (current === 0 ? 4 : 0)}%)`,
  };

  return (
    <View className='index-container'>
      <View className='top'>
        <View className='index-tab'>
          {DEFAULT_TAB_LIST.map(({ tab, title, index }) => (
            <View
              key={tab}
              className={`index_tab_item ${tab} ${
                current === index ? "current" : null
              }`}
              onClick={() => setCurrent(index)}
            >
              {title}
            </View>
          ))}
        </View>
        <View className='scrollbar' style={innerStyle}></View>
      </View>
      {DEFAULT_TAB_LIST[current]["tab"] === "flight" ? (
        <FlightIndex />
      ) : (
        <NoExploit />
      )}
    </View>
  );
}
