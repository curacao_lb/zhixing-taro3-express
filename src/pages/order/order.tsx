import { View, Text, Button, SwiperItem } from '@tarojs/components'
import { useRecoilState } from 'recoil'
import { removeStorageSync } from '@tarojs/taro'

import isLoginHOC from '@/components/loginDecorator'
import userInfo from '@/store/userStore'
import tools from '@/common/tools'
import Tab from '@/components/Tab'
import NoExploit from '@/components/NoExploit'

import OrderList from './list'
import { TAB_LIST } from './constant'

import './order.scss'

const Index = () => {
  const [user, setUser] = useRecoilState(userInfo)
  const { nickName, isLogin, userPhone } = user

  const handleLogin = () => {}

  const handleLogout = () => {
    try {
      removeStorageSync('userInfo')
      tools.showToast({
        title: '登出成功~',
        icon: 'loading',
        duration: 1000
      })
      setUser({ isLogin: false, userPhone: undefined, nickName: undefined })
    } catch (error) {
      tools.showToast('操作失败~')
    }
  }

  return isLogin ? (
    <View className='home-container'>
      <View className='user-box'>
        <Text className='user-name'>欢迎，{nickName || '--'}</Text>
        <Text className='login-out-btn' onClick={handleLogout}>
          退出
        </Text>
      </View>
      <Tab tabList={TAB_LIST} className='tab'>
        {TAB_LIST.map((tab) => {
          return (
            <SwiperItem key={tab.id}>
              {tab.id === 0 ? <OrderList {...{ userPhone }} /> : <NoExploit content='暂无数据' />}
            </SwiperItem>
          )
        })}
      </Tab>
    </View>
  ) : (
    <View className='no-login-container'>
      <Text className='txt'>登录查看订单</Text>
      <Button className='login-btn' onClick={handleLogin}>
        立即登录
      </Button>
    </View>
  )
}

export default isLoginHOC(Index)
