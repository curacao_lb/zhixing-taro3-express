import { Picker, View } from '@tarojs/components'

const BottomPicker = ({
  flightCompanyList,
  currentComponany,
  handleChangeCompany,
  flightList
}): JSX.Element => {
  return (
    <View className={`filter-btn ${flightList.length ? '' : 'hidden'}`}>
      <Picker
        range={flightCompanyList}
        onChange={handleChangeCompany}
        value={currentComponany}
        className='filter-btn'
      >
        筛选
      </Picker>
    </View>
  )
}

export default BottomPicker
