import dayjs from "dayjs"

const weekDay = (date) => {
  const day = dayjs(date).day()
  switch (day) {
    case 1:
      return '周一'
    case 2:
      return '周二'
    case 3:
      return '周三'
    case 4:
      return '周四'
    case 5:
      return '周五'
    case 6:
      return '周六'
    case 0:
      return '周日'
    default:
      return ''
  }
}

export const generateDateList = (): DateItem[] => {
  const MIN_DATE = dayjs().startOf('day');
  const MAX_DATE = MIN_DATE.add(60, 'day');
  const res: DateItem[] = [];
  for (let date = MIN_DATE; date <= MAX_DATE; date = date.add(1, 'day')) {
    res.push({
      dateStr: date.format("YYYY-MM-DD"),
      day: date.format("M-DD"),
      week: weekDay(date.valueOf()),
    });
  }
  return res;
};

export const handleParseCompanyList = (result): string[] => {
  const newResult: string[] = result?.map(({ airCompanyName }) => airCompanyName)

  return [...new Set(['全部', ...newResult])]
}