import { getCurrentInstance, useShareAppMessage, switchTab } from '@tarojs/taro'
import { useEffect, useState } from 'react'
import { Button, View, Text, Image, Input } from '@tarojs/components'
import { useRecoilState } from 'recoil'

import isLoginHOC from '@/components/loginDecorator'
import userInfo from '@/store/userStore'
import tools from '@/common/tools'
import { orderReq } from '@/common/api'
import { ERR_MES } from '@/common/constant'

import { TITLE, IMAGE_URL } from './constant'

import './detail.scss'

const FlightDetail = () => {
  const [data, setData] = useState<any>({})  

  const [user] = useRecoilState(userInfo)
  const { isLogin, nickName, userPhone } = user

  useEffect(() => {
    const { router } = getCurrentInstance()
    setData(router?.params as any)
  }, [])

  useShareAppMessage((res) => {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: TITLE,
      imageUrl: IMAGE_URL
    }
  })

  const handleOrder = () => {
    tools.doLogin(async () => {
      tools.showLoading()
      const result = await orderReq({
        userPhone,
        orderInfo: data
      })
      tools.hideLoading()
      if (result.code === 200) {
        tools.showToast({
          title: '预定成功~',
          icon: 'loading',
          duration: 2000
        }).then(() => {
          switchTab({
            url: '/pages/order/order'
          })
        })

      } else {
        tools.showToast(ERR_MES)
      }
    })
  }

  return (
    <View className='detail-container'>
      <View className='flight-segment'>
        <View className='info-head'>
          <View className='tag'>直飞</View>
          <View className='company-info'>
            <Image src={data.airIcon} className='logo'></Image>
            {`${data.airCompanyName} ${data.dptTimeStr}`}
          </View>
        </View>
        <View className='info-detail'>
          <View className='from'>
            <View className='time'>{data.departureTime}</View>
            <View className='station'>{data.departureAirportName}</View>
          </View>
          <Image className='mid' src='https://i.postimg.cc/z3P1QNf1/1.png'></Image>
          <View className='to'>
            <View className='time'>{data.arrTimeStr}</View>
            <View className='station'>{data.destinationAirportName}</View>
          </View>
        </View>
      </View>
      <View className='passenger-box module-box'>
        <Text className='title'>乘机人</Text>
        {isLogin ? (
          <View className='name'>{nickName}</View>
        ) : (
          <Button className='add-btn name' onClick={tools.doLogin}>
            新增
          </Button>
        )}
      </View>
      <View className='passenger-box module-box'>
        <Text className='title'>联系手机</Text>
        <View className='phone-box'>
          <Text className='num-pre'>+86 </Text>
          <Input disabled placeholder='请输入乘机人手机号' value={userPhone}></Input>
        </View>
      </View>
      <View className='price-item'>
        <View className='color-red'>
          ¥ <Text className='price color-red'>{data.price}</Text>
        </View>
        <View className='order-btn' onClick={handleOrder}>
          订
        </View>
      </View>
      <Button className='share-btn' openType='share'>
        快将行程分享给好友吧
      </Button>
      {/*  机票底部  */}
      <View className='flight-info'></View>
    </View>
  )
}

export default isLoginHOC(FlightDetail)
