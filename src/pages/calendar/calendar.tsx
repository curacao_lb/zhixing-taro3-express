import Taro from '@tarojs/taro'
import { View } from '@tarojs/components'
import { AtCalendar } from 'taro-ui'
import { useRecoilState } from 'recoil'

import flightState from '@/store/fightStore'
import { MIN_DATE, MAX_DATE } from '@/common/constant'

import './calendar.scss'

const Calendar = () => {
  const [fightInfo, setFlightInfo] = useRecoilState(flightState)
  const { dapartureDate } = fightInfo

  const handleChangeSelectDate = ({ value }) => {
    const { start } = value
    setFlightInfo({
      ...fightInfo,
      dapartureDate: start
    })

    Taro.navigateBack()
  }

  return (
    <View className='calendar-page'>
      <AtCalendar
        currentDate={dapartureDate}
        minDate={MIN_DATE}
        maxDate={MAX_DATE}
        onSelectDate={handleChangeSelectDate}
      />
    </View>
  )
}

export default Calendar
