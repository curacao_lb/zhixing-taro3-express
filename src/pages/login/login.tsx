import { navigateBack } from '@tarojs/taro'
import { View, Input, Button } from '@tarojs/components'
import { useState } from 'react'
// import { debounce } from 'lodash'
import { useRecoilState } from 'recoil'

import tools from '@/common/tools'
import userInfo from '@/store/userStore'
import { ERR_MES } from '@/common/constant'
import { loginReq } from '@/common/api'

import './login.scss'

type loginProps = {
  nickName?: string
  userPhone?: string
  password?: string
}

const Login = () => {
  const [loginParam, setLoginParm] = useState<loginProps>({
    nickName: undefined,
    userPhone: undefined,
    password: undefined
  })
  const [, setUser] = useRecoilState(userInfo)

  // const handleInput = debounce((e, type) => {
  //   setLoginParm({
  //     ...loginParam,
  //     [type]: e.detail.value
  //   })
  // }, 300)

  const handleInput = (e, type) => {
    setLoginParm({
      ...loginParam,
      [type]: e.detail.value
    })
  }

  const checkParam = () => {
    const { nickName, userPhone, password } = loginParam
    if (![nickName, userPhone, password].every(Boolean)) {
      return tools.showToast('所有内容必须填写完整~')
    }
    const reg = /^1[3-9]\d{9}$/
    if (!reg.test(userPhone as string)) {
      return tools.showToast('请填写正确的手机号~')
    }

    handleLogin()
  }

  const handleLogin = async () => {
    const { nickName, userPhone, password } = loginParam

    tools.showLoading()

    const response = await loginReq({
      nickName,
      userPhone,
      password
    })
    tools.hideLoading()

    if (response.code === 200) {
      const { result } = response
      setStorage(result)
    } else if (response.code === 401 || response.code === 0) {
      tools.showToast(response.message)
    } else {
      tools.showToast(ERR_MES)
    }

    function setStorage(result) {
      const { userPhone: phone, nickName: name } = result
      tools.setStorageSyncWithTime(
        'userInfo',
        {
          userPhone: phone,
          nickName: name
        },
        10
      )

      setUser({
        isLogin: !!phone,
        userPhone: phone,
        nickName: name
      })

      navigateBack()
    }
  }

  return (
    <View className='login-container'>
      <View className='login-top'>
        <View>你好，</View>
        <View>欢迎登录</View>
      </View>
      <View className='login-box'>
        <Input
          type='text'
          className='nick-name input'
          placeholder='请输入昵称'
          placeholderClass='placeholer-class'
          onInput={(e) => handleInput(e, 'nickName')}
        ></Input>
        <Input
          type='text'
          className='phone input'
          placeholder='请输入手机号'
          placeholderClass='placeholer-class'
          onInput={(e) => handleInput(e, 'userPhone')}
        ></Input>
        <Input
          type='safe-password'
          className='password input'
          placeholder='请输入密码'
          placeholderClass='placeholer-class'
          onInput={(e) => handleInput(e, 'password')}
        ></Input>
      </View>
      <Button className='login-btn' onClick={checkParam}>
        登录
      </Button>
    </View>
  )
}

export default Login
