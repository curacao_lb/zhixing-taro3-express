interface dptOptions extends loadingDptOptions, toastDptOptions { }

export const judgeType = (dptOptions: dptOptions, param: showLoadingProps) => {
  if (Object.prototype.toString.call(param) === '[object String]') {
    dptOptions.title = param as string
  } else if (Object.prototype.toString.call(param) === '[object Object]') {
    dptOptions = {
      ...dptOptions,
      ...param as object
    }
  } else {
    throw new Error('参数类型有误，应该是字符串或者对象')
  }

  return dptOptions
}

export const objToString = (obj: Object) => {
  let searchKeys: string[] = []
  if (Object.prototype.toString.call(obj) === "[object Object]" && Object.keys(obj).length) {
    for (let key in obj) {
      searchKeys.push(`${key}=${obj[key]}`)
    }
  }

  return searchKeys.join('&')
}