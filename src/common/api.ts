import tools from "../common/tools";

const API_PRE: string = tools.isH5() ? '' : 'http://localhost:3000'

// 营销广告
export const adsReq = (params?: object): Promise<any> => tools.request({
  url: `${API_PRE}/ads/advertising`,
  params
})

// 城市列表页
export const cityReq = (params?: object): Promise<any> => tools.request({
  url: `${API_PRE}/city/airPortList`,
  params
})

// 航班列表页
export const flightListReq = (params?: object): Promise<any> => tools.request({
  url: `${API_PRE}/list/singleList`,
  params
})

// 查询订票
export const loginReq = (params?: object): Promise<any> => tools.request({
  url: `${API_PRE}/login`,
  params,
  method: 'POST'
})

// 预定机票
export const orderReq = (params?: object): Promise<any> => tools.request({
  url: `${API_PRE}/order/order`,
  params,
  method: 'POST'
})

// 查询预定的机票
export const getOrderListReq = (params?: object): Promise<any> => tools.request({
  url: `${API_PRE}/order/getOrderList`,
  params,
  method: 'POST'
})
