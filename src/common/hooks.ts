import { useRef, useEffect } from "react"

export const useUpdateEffect = (fn: Function, inputs: any[] = []) => {
  const didMountRef = useRef<boolean>(false)
  useEffect(() => {
    if (didMountRef.current) fn()
    else didMountRef.current = true
  }, inputs)
}