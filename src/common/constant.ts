import dayjs from 'dayjs'

export const ERR_MES: string = "网络开了小差，请稍后重试~"

// 日历最小日期
export const MIN_DATE: string = dayjs().format('YYYY-MM-DD')
// 最大日期
export const MAX_DATE: string = dayjs().add(60, 'day').format('YYYY-MM-DD')