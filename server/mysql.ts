import { PASSWORD, USER_NAME, DATABASE_NAME } from '../globalConfig'

const mysql = require('mysql2')

const options = {
  host: '127.0.0.1', //主机名
  port: '3306',
  user: USER_NAME, // 数据库用户名
  password: PASSWORD,
  database: DATABASE_NAME // 数据库名称
}

// 创建数据库连接
const connection = mysql.createConnection(options)

// 建立连接
connection.connect((err) => {
  if (err) {
    console.log(err)
    return
  }

  console.log('数据库连接成功')
})

// 执行 mysql 查询命令
// connection.query("SELECT * from teacher_table", (err, res) => {
//   console.log(res)
// })

// 封装公共方法查询数据库
/**
 * mysql公共的查询方法
 * @{param} strSql 执行 sql 命令
 */
const sqlQuery = (strSql: string): Promise<any> => {
  return new Promise((resolve, reject) => {
    connection.query(strSql, (err, res) => {
      if (err) {
        reject(err)
        return
      }

      resolve(res)
    })
  })
}

export default sqlQuery