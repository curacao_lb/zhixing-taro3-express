import request from 'request'
import sqlQuery from '../mysql'


// 创建机场城市列表
request(
  "https://www.brown77.cn/city/airportList",
  { json: true },
  async (_err, _res, body) => {
    const createTableSql = `
    CREATE TABLE IF NOT EXISTS airport_list (
      id INT NOT NULL AUTO_INCREMENT,
      cityName CHAR(50) NOT NULL,
      cityId int NOT NULL,
      firstLetter CHAR(50) NOT NULL,
      airportName CHAR(50) NOT NULL,
      PRIMARY KEY (id)
    ) ENGINE=InnoDB;
  `
    await sqlQuery('DROP TABLE IF EXISTS airport_list')
    await sqlQuery(createTableSql)

    body.result.map(async ({ id, cityName, cityId, firstLetter, airportName }) => {
      const insertSql = `
      INSERT INTO airport_list(id, cityName, cityId, firstLetter, airportName) values(${id}, '${cityName}', ${cityId}, '${firstLetter}', '${airportName}')
      `
      await sqlQuery(insertSql)
    })
  }
)