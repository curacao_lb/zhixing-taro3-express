// 建表
export const createUserTabelSql = `
  CREATE TABLE IF NOT EXISTS user (
    id int not null auto_increment,
    userPhone char(11) not null,
    password char(10) not null,
    nickName char(50) not null,
    PRIMARY KEY (id)
  ) ENGINE=InnoDB;
`
// 查询表中是否有对应的手机号
export const queryUserPhoneExistence = (userPhone) => {
  return `
    SELECT * FROM user WHERE userPhone='${userPhone}'
  `
}

// 更新用户名称
export const updateUserNickName = (nickName, userPhone) => {
  return `
    UPDATE user SET nickName='${nickName}' WHERE userPhone='${userPhone}'
  `
}

// 注册一个新的账户
export const register = (nickName, userPhone, password) => {
  return `
    INSERT INTO user(id, nickName, userPhone, password) VALUES (null, '${nickName}', '${userPhone}', '${password}')
  `
}