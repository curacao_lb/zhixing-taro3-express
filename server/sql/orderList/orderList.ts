// 建表
export const createOrderListTableSql = `
  CREATE TABLE IF NOT EXISTS orderList (
    id int not null auto_increment,
    userPhone char(11) not null,
    departureCityName char(50) not null,
    destinationCityName char(50) not null,
    dptTimeStr char(50) not null,
    arrTimeStr char(50) not null,
    price decimal not null,
    PRIMARY KEY (id)
  ) ENGINE=InnoDB;
`

export const insertOrderList = (values) => {
  const { userPhone, departureCityName, destinationCityName, dptTimeStr, arrTimeStr, price } = values
  return `
    INSERT INTO orderList(id, userPhone, departureCityName, destinationCityName, dptTimeStr, arrTimeStr, price) VALUES (null, '${userPhone}', '${departureCityName}', '${destinationCityName}', '${dptTimeStr}', '${arrTimeStr}', '${price}')
  `
}