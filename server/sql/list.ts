import request from 'request'
import sqlQuery from '../mysql'



request("https://www.brown77.cn/list/singleList", {json: true}, async (_err, _res, body) => {
  const strSql1 =
  `
    create table flight_list(
      id int not null auto_increment,
      arrTime char(50) not null,
      airCompanyName char(50) not null,
      airIcon char(50) not null,
      price int not null,
      dptTimeStr char(50) not null,
      arrTimeStr char(50) not null,
      primary key (id)
    ) engine=innodb;
  `
  // 删除表
  await sqlQuery(`drop table if exists flight_list`)
  await sqlQuery(strSql1)
  for (let i = 0; i < body.result.length; i++) {
    const { id, arrTime, airCompanyName, airIcon, price, dptTimeStr, arrTimeStr } = body.result[i]
    const strSql2 = `insert into flight_list(id, arrTime, airCompanyName, airIcon, price, dptTimeStr, arrTimeStr) values ('${id}', '${arrTime}', '${airCompanyName}', '${airIcon}', '${price}', '${dptTimeStr}', '${arrTimeStr}');`
    await sqlQuery(strSql2)
  }
})