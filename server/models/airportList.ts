import { Request, Response, Router } from 'express'
import sqlQuery from '../mysql'


const router: Router = Router()

function sortCityList(result) {
  if (!Array.isArray(result)) return
  return result.sort((a, b) => {
    if (a.firstLetter < b.firstLetter) {
      return -1;
    } else if (a.firstLetter > b.firstLetter) {
      return 1;
    } else {
      return 0;
    }
  })
}

router.get('/airPortList', async (_req: Request, res: Response) => {
  const strSql = 'SELECT * FROM airport_list'
  try {
    let result = await sqlQuery(strSql)

    result = sortCityList(result)

    res.send({
      code: 200,
      message: '请求成功',
      result
    })
  } catch (error) {
    res.send({
      code: 0,
      message: '请求失败'
    })
  }
})

export default router
