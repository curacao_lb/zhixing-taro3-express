import { Application } from 'express'

import adsRouter from './ads'
import singleListRouter from './list'
import cityRouter from './airportList'
import loginRouter from './login'
import orderListRouter from './orderList/orderList'
import getOrderListRouter from './orderList/getOrderList'

export default (app: Application) => {
  // express 使用中间件的方法
  // path: 路径，Callback：回调
  app.use('/ads', adsRouter)
  app.use('/city', cityRouter)
  app.use('/list', singleListRouter)
  app.use(loginRouter)
  app.use('/order', orderListRouter)
  app.use('/order', getOrderListRouter)
}