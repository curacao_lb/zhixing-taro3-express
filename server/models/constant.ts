/**
 * 得到一个两数之间的随机整数
 */
export const randomPrice = (min: number, max: number): number => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //不含最大值，含最小值
}

/**
 *  
 */
export const sortCityList = (result:Promise<any>) => {
  if (!Array.isArray(result)) return
  return result.sort((a, b) => {
    if (a.firstLetter < b.firstLetter) {
      return -1;
    } else if (a.firstLetter > b.firstLetter) {
      return 1;
    } else {
      return 0;
    }
  })
}