import { Request, Response, Router } from 'express'
import sqlQuery from '../mysql'

const router: Router = Router()

router.get('/advertising', async (_req: Request, res: Response) => {
  const strSql: string = `SELECT * FROM ads `
  try {
    const result: Promise<any> = await sqlQuery(strSql)
    res.send({
      code: 200,
      message: '请求成功',
      result
    })
  } catch {
    res.send({
      code: 0,
      message: '请求失败'
    })
  }
})

export default router

