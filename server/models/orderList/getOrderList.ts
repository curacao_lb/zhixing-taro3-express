import { Request, Response, Router } from 'express'

import sqlQuery from '../../mysql'
import { queryOrderSql } from '../../sql/orderList/getOrderList'

const router: Router = Router()

// 查询订单
router.post('/getOrderList', async (req: Request, res: Response) => {
  try {
    const { userPhone } = req.body
    const result = await sqlQuery(queryOrderSql(userPhone))

    res.send({
      code: 200,
      message: '查询成功',
      result
    })

  } catch (error) {
    res.send({
      code: 0,
      message: '请求失败',
      result: error
    })
  }
})

export default router