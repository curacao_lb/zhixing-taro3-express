import { Request, Response, Router } from 'express'

import sqlQuery from '../../mysql'
import { createOrderListTableSql, insertOrderList } from '../../sql/orderList/orderList'

const router: Router = Router()

router.post('/order', async (req: Request, res: Response) => {
  try {
    const { userPhone, orderInfo } = req.body
    const insertValues = {
      ...orderInfo,
      userPhone
    }

    await sqlQuery(createOrderListTableSql)
    await sqlQuery(insertOrderList(insertValues))

    res.send({
      code: 200,
      message: '预定成功',
    });

  } catch (error) {
    res.send({
      code: 0,
      message: '请求失败',
      result: error
    })
  }
})

export default router