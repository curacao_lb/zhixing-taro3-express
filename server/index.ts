import express, { Application } from 'express'
import models from './models/index'

const app: Application = express()

const PORT: number = 3000

// post请求中从 req.body 中拿到请求参数
// 当请求体的 content-type 是 application/json 时
app.use(express.json())
// // 当请求体的 content-type 是 application/x-www-form-urlencoded 时
app.use(express.urlencoded({
  extended: true
}))

models(app)

app.listen(PORT, () => {
  console.log(`listening on ${PORT}`);
})