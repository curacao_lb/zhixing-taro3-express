interface TabArray {
  label: string;
  id: number;
}

interface loadingDptOptions {
  title: string,
  mask: boolean
}

type ToastIcon = 'success' | 'error' | 'loading' | 'none'

type showLoadingProps = string | object | undefined

interface toastDptOptions {
  title: string,
  icon?: ToastIcon,
  mask: boolean,
  duration?: number
}

interface DateItem {
  dateStr: string;
  day: string;
  week: string;
}

interface OrderProps {
  id: number,
  userPhone: number,
  departureCityName: string,
  destinationCityName: string,
  dptTimeStr: string,
  arrTimeStr: string,
  price: number
}